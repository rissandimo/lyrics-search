// dom elements
const form = document.getElementById('form');
const moreResults = document.getElementById('more-results');
const resultsContainer = document.getElementById('result-container');
const search = document.getElementById('search');

//properties
const apiURL = 'https://api.lyrics.ovh';
const lyricsApiUrl = 'https://api.lyrics.ovh/v1';

//fetch songs or artists from API
async function fetchSongs(e){
    e.preventDefault();
    const query = search.value;

    //fetch search query and convert to json
    const response = await fetch(`${apiURL}/suggest/${query}`);
    const data = await response.json();
    
    renderDataToUi(data);
}

async function getLyrics(artistName, songTitle){
    const response = await fetch(`${lyricsApiUrl}/${artistName}/${songTitle}`);
    const data = await response.json();

    const lyrics = data.lyrics.replace(/(\r\n|\r|\n)/g, '<br>');

    moreResults.innerHTML = `
    
    <h2>Lyrics</h2>
    ${lyrics}`;
    
}

async function getMoreSongs(url){ // prev or next

    const response = await fetch(`https://cors-anywhere.herokuapp.com/${url}`);
    const data = await response.json();

    renderDataToUi(data);
}

function renderDataToUi(data){
    const songData = data.data;
    console.log(songData);
    

    //traverse through songs array to get artist name and song title
    resultsContainer.innerHTML = `
    <ul class="songs">
    ${songData.map(song =>
        `
        <li>
            <span><strong>${song.artist.name}</strong> - ${song.album.title}</span>
            <button class="btn" data-artist="${song.artist.name}" data-songTitle="${song.album.title}">Get Lyrics</button>
        </li>
        `
        ).join('')}
</ul>
    `;

    //if there are more songs - show them
    if(data.prev || data.next){
        moreResults.innerHTML = `
        ${
            data.prev
              ? `<button class="btn" onclick="getMoreSongs('${data.prev}')">Prev</button>`
              : ''
          }
          ${
            data.next
              ? `<button class="btn" onclick="getMoreSongs('${data.next}')">Next</button>`
              : ''
          }        
        `;
    }
    else{
        moreResults.innerHTML = '';
    }
  
}




                                                    //event listeners

// artist/song query                                                    
form.addEventListener('submit', fetchSongs);

//get lyrics
resultsContainer.addEventListener('click', e => {
    const clickedElement = e.target;

    if(clickedElement.tagName === 'BUTTON'){
        let artistName = clickedElement.getAttribute('data-artist');
        let songTitle = clickedElement.getAttribute('data-songTitle');

        getLyrics(artistName, songTitle);
    }
    
})

